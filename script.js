document.addEventListener('DOMContentLoaded', function(){
    
    let params = new URLSearchParams(document.location.search);
    let isbot = params.get('isbot');
    
    if(isbot == "1"){
        
        let script = document.createElement('script');
        script.src = "https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit";
        document.head.append(script);
        
        document.querySelector("button.t-submit").setAttribute("disabled", true);
        document.querySelector('.t-form__submit').style.position = "relative";
        
        document.querySelector('.t-input-group_ph').insertAdjacentHTML('afterend', '<div id="captcha-container-1"></div>');	
        document.querySelector('.t-form__submit').insertAdjacentHTML('afterBegin', '<div id="fake-button" style="position: absolute; width: 100%; height: 100%; z-index: 99; cursor: pointer;"></div>');
        
        
        document.getElementById('fake-button').addEventListener('click', event => {
            var response = grecaptcha.getResponse();
            if(response.length == 0) {
                alert('Вы не прошли проверку Google ReCaptcha, попробуйте еще раз');
                event.preventDefault();	
                return false;
            }else{
                document.querySelector("button.t-submit").removeAttribute("disabled");
                $("button.t-submit").trigger('click');
                document.querySelector("button.t-submit").setAttribute("disabled", true);
            }
        });
        
    }	
        
}, false);

var onloadCallback = function(){
        var key = '6LdyWfEpAAAAAAElkGDxYTxSxxxxxxxxx';
        grecaptcha.render('captcha-container-1', {
            'sitekey': key
        });
};
