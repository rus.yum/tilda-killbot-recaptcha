# Tilda + Killbot + Google Recaptha
Реализация "выборочной" рекапчи для пользователей и ботов потребует следующих шагов:
- Получаем параметр isbot из URL
- Если isbot = 1, то загружаем скрипт Google Recaptcha на страницу.
- Добавляем блок с рекапчей в форму и рендерим рекапчу.
- Деактивируем button с submit, добавляем поверх блок с position:absolute и отлавливаем клики.
- Проверяем при нажатии есть ли grecaptcha.getResponse() и если его нет - выводим alert с предложением пройти проверку.
- В случае, если grecaptcha.getResponse() есть - убираем деактивацию button и делаем клик по кнопке, после этого снова деактивируем.

## Разберем код из файла script.js

Данный js скрипт необходимо добавить на страницу Tilda в блок Html-код заключив в <script></script>

```javascript
document.addEventListener('DOMContentLoaded', function(){ /*Запускаем после того как DOM прогружен*/
		
		let params = new URLSearchParams(document.location.search); /*Создаем экземляр URLSearchParams и передаем переменной isbot параметр url isbot*/
		let isbot = params.get('isbot');
		
		if(isbot == "1"){ /*Проверяем значение параметра isbot, 1 - бот*/
			
            /*Загружем скрипт Google ReCaptcha с нужными параметрами*/
			let script = document.createElement('script');
			script.src = "https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit";
			document.head.append(script);
			
            /*Делаем button неактивной и прописываем родительскому блоку position:relative, чтобы вложенный absolute нормально работал*/
			document.querySelector("button.t-submit").setAttribute("disabled", true);
			document.querySelector('.t-form__submit').style.position = "relative";
			
            /*Вставляем в DOM блок для отрисовки капчи и блок поверх button, чтобы по нему ослеживать клики, так как disabled не вызывает событий click*/
			document.querySelector('.t-input-group_ph').insertAdjacentHTML('afterend', '<div id="captcha-container-1"></div>');	
			document.querySelector('.t-form__submit').insertAdjacentHTML('afterBegin', '<div id="fake-button" style="position: absolute; width: 100%; height: 100%; z-index: 99; cursor: pointer;"></div>');
			
			/*Отслеживаем нажатие на button, а точнее на фейковый блок*/
			document.getElementById('fake-button').addEventListener('click', event => {
				var response = grecaptcha.getResponse(); /*Выполняем grecaptcha.getResponse(), чтобы понять пройдена ли проверка капчи*/
				if(response.length == 0) { /*Если не пройдена то вызываем alert с оповещением и возвращаем на всякий случай false и устанавливаем event.preventDefault(), чтобы точно ничего не отработало, но можно и без них*/
					alert('Вы не прошли проверку Google ReCaptcha, попробуйте еще раз');
					event.preventDefault();	
					return false;
				}else{ /*При успешной проверке - выполняем этот код*/
					document.querySelector("button.t-submit").removeAttribute("disabled");/*Активируем button*/
					$("button.t-submit").trigger('click'); /*Кликаем по нему*/
					document.querySelector("button.t-submit").setAttribute("disabled", true);/*Снова деактивируем*/
				}
			});
			
		}	
			
	}, false);

    /*Функция рендера капчи, необходимо указать открытый ключ из настроек рекапчи. Почему-то не хочет работать внутри условия.*/
    var onloadCallback = function(){
			var key = '6LdyWfEpAAAAAAElkGDxYTxS5TfXXXXXXXXXX';
			grecaptcha.render('captcha-container-1', { /*Указываем  id блока, который выше добавили в DOM для капчи*/
				'sitekey': key
			});
		};
```